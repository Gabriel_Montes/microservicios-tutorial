package com.carro.service.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.carro.service.entities.Carro;

@Repository
public interface CarroRepository extends JpaRepository<Carro, Integer>{
	
	List<Carro> findByUserId(int usuarioId);

}
