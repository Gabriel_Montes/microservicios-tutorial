package com.carro.service.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carro.service.entities.Carro;
import com.carro.service.servicios.CarroService;

@RestController
@RequestMapping("/carros")
public class CarroController
{
	@Autowired
	private CarroService carroService;
	
	@GetMapping
	public ResponseEntity<List<Carro>> getCarros()
	{
		List<Carro> carros = carroService.getAll();
		
		if(carros.isEmpty())
		{
			return ResponseEntity.noContent().build();
		}
		
		return ResponseEntity.ok(carros);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Carro> getCarro(@PathVariable("id") int id)
	{
		Carro carro = carroService.getCarroById(id);
		
		if(carro == null)
		{
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(carro);
	}
	
	@PostMapping
	public ResponseEntity<Carro> saveCarro(@RequestBody Carro carro)
	{
		Carro carroSaved = carroService.save(carro);
		
		return ResponseEntity.ok(carroSaved);
	}
	

	@GetMapping("usuario/{id}")
	public ResponseEntity<List<Carro>> getCarrosPorCliente(@PathVariable("id") int id)
	{
		List<Carro> carros = carroService.byUserId(id);
		
		if(carros.isEmpty())
		{
			return ResponseEntity.noContent().build();
		}
		
		return  ResponseEntity.ok(carros);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Carro> deleteCarro(@PathVariable("id") int id)
	{
		return null; // ResponseEntity.ok();
	}
	
}
