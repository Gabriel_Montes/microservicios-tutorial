package com.carro.service.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carro.service.entities.Carro;
import com.carro.service.repositorio.CarroRepository;

@Service
public class CarroService { 
	
	@Autowired CarroRepository carroRepository;
	
	public List<Carro> getAll()
	{
		return carroRepository.findAll();
	}
	
	public Carro getCarroById(int id)
	{
		return carroRepository.findById(id).orElse(null);
	}
	
	public Carro save(Carro user)
	{
		return carroRepository.save(user);
	}
	
	public List<Carro> byUserId(int usuarioId)
	{
		return carroRepository.findByUserId(usuarioId);
	}
}