package com.usuario.service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usuario.service.entity.User;
import com.usuario.service.modelos.Carro;
import com.usuario.service.modelos.Moto;
import com.usuario.service.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> userList()
	{
		List<User> users = userService.getAll();
		
		if(users.isEmpty())
		{
			return ResponseEntity.noContent().build();
		}
		
		return  ResponseEntity.ok(users);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable("id") int id)
	{
		User user = userService.getUserById(id);
		
		if(user == null)
		{
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(user);
	}
	
	@PostMapping
	public ResponseEntity<User> saveUser(@RequestBody User user)
	{
		User userSaved = userService.save(user);
		
		return ResponseEntity.ok(userSaved);
	}
	
	
	@GetMapping("/carros/{id}")
	public ResponseEntity<List<Carro>> listarCarros(@PathVariable("id") int id)
	{
		User user = userService.getUserById(id);
		
		if(user == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		List<Carro> carros = userService.getCarros(id);

		return ResponseEntity.ok(carros);
	}
	
	
	@GetMapping("/motos/{id}")
	public ResponseEntity<List<Moto>> listarMotos(@PathVariable("id") int id)
	{
		User user = userService.getUserById(id);
		
		if(user == null)
		{
			return ResponseEntity.notFound().build();
		}
		
		List<Moto> motos = userService.getMotos(id);
		
		return ResponseEntity.ok(motos);
	}
	
	
	
	
	@DeleteMapping("/{id}")
	public ResponseEntity<User> deleteUser(@PathVariable("id") int id)
	{
		return null; // ResponseEntity.ok();
	}
	
	
	//Feign
	@PostMapping("/carros/{usuarioId}")
	public ResponseEntity<Carro> guardarCarro(@PathVariable("usuarioId") int usuarioId, @RequestBody Carro carro)
	{
		Carro nuevoCarro = userService.save(usuarioId, carro);
		return ResponseEntity.ok(nuevoCarro);
	}

	@PostMapping("/motos/{usuarioId}")
	public ResponseEntity<Moto> guardarMoto(@PathVariable("usuarioId") int usuarioId, @RequestBody Moto moto)
	{
		Moto nuevaMoto = userService.save(usuarioId, moto);
		return ResponseEntity.ok(nuevaMoto);
	}
	
	@GetMapping("/carros-motos/{usuarioId}")
	public ResponseEntity<Map<String, Object>> consultarCarrosMotosxUsuario(@PathVariable int usuarioId)
	{
		Map<String, Object> resultado = userService.getMotosCarrosxUsuario(usuarioId);
		return ResponseEntity.ok(resultado);
	}
	
	
}
