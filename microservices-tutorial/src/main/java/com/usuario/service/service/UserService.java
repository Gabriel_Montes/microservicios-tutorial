package com.usuario.service.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.usuario.service.repository.UserRepository;

import lombok.extern.slf4j.Slf4j;

import com.usuario.service.entity.User;
import com.usuario.service.feignclients.CarroFeignClient;
import com.usuario.service.feignclients.MotoFeignClient;
import com.usuario.service.modelos.Carro;
import com.usuario.service.modelos.Moto;

@Slf4j
@Service
public class UserService {

	@Autowired 
	private RestTemplate restTemplate;
	
	@Autowired 
	private UserRepository userRepository;
	
	@Autowired
	private CarroFeignClient carroFeignClient;
	
	
	//RestTemplate
		public List<Carro> getCarros(int usuarioId)
		{
			
			log.info("entrando");
			List<Carro> carros = restTemplate.getForObject("http://localhost:8082/carros/usuario/" + usuarioId, List.class );
	
	//		System.out.println(carros.get(1));
			//carros.forEach(c -> log.info(c.getMarca()));
			
			return carros;
		}
		
		public List<Moto> getMotos(int usuarioId)
		{
			List<Moto> motos = restTemplate.getForObject("http://localhost:8083/motos/usuario/" + usuarioId, List.class );
			return motos;
		}
		
	public Carro save(int usuarioId, Carro carro)
	{
		carro.setUserId(usuarioId);
		Carro nuevoCarro = carroFeignClient.saveCarro(carro);
		return nuevoCarro;
	}
	
	@Autowired
	private MotoFeignClient motoFeignClient;
	
	public Moto save(int usuarioId, Moto moto)
	{
		moto.setUsuarioId(usuarioId);
		Moto nuevaMoto = motoFeignClient.save(moto);
		return nuevaMoto;
	}
	
	public Map<String, Object> getMotosCarrosxUsuario(int usuarioId)
	{
		Map<String, Object> resultado = new HashMap<>();
		User user = userRepository.findById(usuarioId).orElse(null);
		
		if(user == null)
		{
			resultado.put("Mensaje", "El usuario no existe");
			return resultado;
		}
		
		resultado.put("Usuario", user);
		List<Carro> carros = carroFeignClient.getCarros(usuarioId);
		if(carros.isEmpty())
		{
			resultado.put("Carros", "El usuario no tiene Carros.");
		}
		else
		{
			resultado.put("Carros", carros);			
		}
		List<Moto> motos = motoFeignClient.getMotos(usuarioId);
		
		if(motos.isEmpty())
		{
			resultado.put("Motos", "El usuario no tiene Motos.");
		}
		else
		{
			resultado.put("Motos", motos);			
		}
		return resultado;
		
	}
	
	
	public List<User> getAll()
	{
		return userRepository.findAll();
	}
	
	public User getUserById(int id)
	{
		return userRepository.findById(id).orElse(null);
	}
	
	public User save(User user)
	{
		return userRepository.save(user);
	}
	
//	public void detele()
//	{
//		
//	}
}
